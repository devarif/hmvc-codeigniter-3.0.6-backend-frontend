This Application Using HMVC Codeigniter version.3.0.6

HMVC-CodeIgniter-3.0.6
CodeIgniter-3.0.6 and Modular Extensions - HMVC.

Installation HMVC CodeIgniter
Download HMVC CodeIgniter 
Extract to your localhost
Go to url: localhost/HMVC-CodeIgniter-3.0.6
Enjoy

Structur Directory
==FRONTEND==
+Frontend
--controllers
--models
--views

==BACKEND==
+Backend
--controllers
--models
--views

url
==FRONTEND==
--USER--
site_url();
site_url('home')
site_url('product')
site_url('product/blablabla')
--MEMBER--
site_url('member');
site_url('member/login')
site_url('member/blablabla')

==BACKEND==
--ADMIN--
site_url('admin');
site_url('admin/login');
site_url('admin/dashboard');
site_url('admin/product');
site_url('admin/product/blablabla')

url example: localhost/hmvc-codeigniter-3.0.6-backend-frontend/admin/dashboard

author: ariefbruce
thanks to : https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc