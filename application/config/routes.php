<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontend/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/*
| -------------------------------------------------------------------------
| FRONTEND
| -------------------------------------------------------------------------
*/
/* USER */
$route['home'] = 'frontend/home';
$route['product'] = 'frontend/product';
$route['product/(.*)'] = 'frontend/product/$1';
/* MEMBER */
$route['member'] = 'frontend/login';
$route['member/login'] = 'frontend/login';
$route['member/profile'] = 'frontend/profile';
/*
| -------------------------------------------------------------------------
| BACKEND
| -------------------------------------------------------------------------
*/
/* ADMIN */
$route['admin'] = 'backend/login';
$route['admin/login'] = 'backend/login';
$route['admin/login/process'] = 'backend/login/process';
$route['admin/forgot-password'] = 'backend/forgot_password';
$route['admin/forgot-password/process'] = 'backend/forgot_password/process';
$route['admin/dashboard'] = 'backend/dashboard';
$route['admin/product'] = 'backend/product';
$route['admin/product/(.*)'] = 'backend/product/$1';
$route['admin/order'] = 'backend/order';
$route['admin/order/(.*)'] = 'backend/order/$1';
$route['admin/post'] = 'backend/post';
$route['admin/post/(.*)'] = 'backend/post/$1';
$route['admin/user'] = 'backend/user';
$route['admin/customer'] = 'backend/user/customer';
$route['admin/customer/detail'] = 'backend/user/customer_detail';
$route['admin/customer/edit'] = 'backend/user/customer_edit';
$route['admin/staff'] = 'backend/user/staff';
$route['admin/staff/detail'] = 'backend/user/staff_detail';
$route['admin/staff/add'] = 'backend/user/staff_add';
$route['admin/staff/edit'] = 'backend/user/staff_edit';
$route['admin/user/(.*)'] = 'backend/user/$1';
$route['admin/category'] = 'backend/category';
$route['admin/category/(.*)'] = 'backend/category/$1';
$route['admin/page'] = 'backend/page';
$route['admin/page/(.*)'] = 'backend/page/$1';
$route['admin/confirmation'] = 'backend/confirmation';
$route['admin/confirmation/(.*)'] = 'backend/confirmation/$1';
$route['admin/media'] = 'backend/media';
$route['admin/media/(.*)'] = 'backend/media/$1';
$route['admin/ads'] = 'backend/ads';
$route['admin/ads/(.*)'] = 'backend/ads/$1';
$route['admin/slide'] = 'backend/slide';
$route['admin/slide/(.*)'] = 'backend/slide/$1';
$route['admin/mail/inbox'] = 'backend/mail';
$route['admin/mail/read'] = 'backend/mail/read_mail';
$route['admin/mail/(.*)'] = 'backend/mail/$1';
$route['admin/setting'] = 'backend/setting';