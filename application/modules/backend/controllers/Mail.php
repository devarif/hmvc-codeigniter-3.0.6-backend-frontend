<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
		$this->load->model(array('backend/m_mail'));
		// $this->load->library(array());
		// $this->load->helper(array());
	}

	public function index()
	{
		$data['title']='Mail Page';
		$this->theme->backend('mail-inbox', $data);
	}
	public function sent()
	{
		$data['title']='Mail Page';
		$this->theme->backend('mail-sent', $data);
	}
	public function drafts()
	{
		$data['title']='Mail Page';
		$this->theme->backend('mail-drafts', $data);
	}
	public function trash()
	{
		$data['title']='Mail Page';
		$this->theme->backend('mail-trash', $data);
	}
	public function read_mail()
	{
		$data['title']='Detail Mail';
		$this->theme->backend('mail-read', $data);
	}
	public function compose()
	{
		$data['title']='Add Mail';
		$this->theme->backend('mail-compose', $data);
	}
	public function reply()
	{
		$data['title']='Edit Mail';
		$this->theme->backend('mail-reply', $data);
	}
	public function delete()
	{
		//your code
	}
	public function delete_all()
	{
		//your code
	}
}
