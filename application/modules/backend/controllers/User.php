<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
		$this->load->model(array('backend/m_user'));
		// $this->load->library(array());
		// $this->load->helper(array());
	}

	public function index()
	{
		redirect('user/customer');
	}
	public function customer()
	{
		$data['title']='Customer Page';
		$this->theme->backend('customer', $data);
	}
	public function customer_detail()
	{
		$data['title']='Detail Customer Page';
		$this->theme->backend('detail-customer', $data);
	}
	public function customer_edit()
	{
		$data['title']='Edit Customer Page';
		$this->theme->backend('edit-customer', $data);
	}
	public function staff()
	{
		$data['title']='Staff Page';
		$this->theme->backend('staff', $data);
	}
	public function staff_detail()
	{
		$data['title']='Detail Staff Page';
		$this->theme->backend('detail-staff', $data);
	}
	public function staff_add()
	{
		$data['title']='Add Staff Page';
		$this->theme->backend('add-staff', $data);
	}
	public function staff_edit()
	{
		$data['title']='Edit Staff Page';
		$this->theme->backend('edit-staff', $data);
	}
}
