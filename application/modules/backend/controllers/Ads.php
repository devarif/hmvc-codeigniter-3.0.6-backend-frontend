<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ads extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		// Your own constructor code
		$this->load->model(array('backend/m_ads'));
		// $this->load->library(array());
		// $this->load->helper(array());
	}

	public function index()
	{
		$data['title']='Ads Page';
		$this->theme->backend('ads', $data);
	}
	public function detail()
	{
		$data['title']='Detail Ads';
		$this->theme->backend('detail-ads', $data);
	}
	public function add()
	{
		$data['title']='Add Ads';
		$this->theme->backend('add-ads', $data);
	}
	public function edit()
	{
		$data['title']='Edit Ads';
		$this->theme->backend('edit-ads', $data);
	}
	public function delete()
	{
		//your code
	}
	public function delete_all()
	{
		//your code
	}
}
