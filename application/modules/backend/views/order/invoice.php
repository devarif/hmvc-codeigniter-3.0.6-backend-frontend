        <!-- <div class="pad-5 margin no-print">
          <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Note:</h4>
            This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
          </div>
        </div> -->

        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-file-text-o"></i> Invoice # <?php //echo $invoice['kode_order']; ?>
                <small class="pull-right">Date: <?php //$date = date_create($invoice['tgl_transaksi']); echo date_format($date, 'H:i d-m-Y'); ?></small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              From
              <address>
                <strong><?php //echo $invoice['f_nama'].' '.$invoice['l_nama']; ?></strong><br>
                <?php ////echo $invoice['alamat']; ?><br>
                <?php //echo $invoice['kecamatan'].' ,'; ?> <?php //echo $invoice['kabupaten']; ?><br>
                <?php //echo $invoice['provinsi'].' ,'; ?> <?php //echo $invoice['kode_pos']; ?><br>
                Phone: <?php //echo $invoice['no_hp']; ?><br>
                Email: <?php //echo $invoice['email']; ?>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              To
              <address>
                <strong><?php //echo $invoice['nm']; ?></strong><br>
                <?php //echo $invoice['almt']; ?><br>
                <?php //echo $invoice['kec'].' ,'; ?> <?php //echo $invoice['kab']; ?><br>
                <?php //echo $invoice['prov'].' ,'; ?> <?php //echo $invoice['kp']; ?><br>
                Phone: <?php //echo $invoice['hp']; ?><br>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <b>Invoice # <?php //echo $invoice['kode_order']; ?></b><br>
              <br>
              <b>Order ID:</b> <?php //echo $invoice['kode_order']; ?><br>
              <b>Status:</b> <?php //echo $invoice['status_order']; ?>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row" style="margin-top: 15px;">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 50px;">No</th>
                    <th style="width: 350px;">Item</th>
                    <th class="text-center" style="width: 50px;">Quantity</th>
                    <th style="width: 100px;">Item Price</th>
                    <th style="width: 100px;">Sub Total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php //$i=1; foreach ($invoice_product as $row) { ?>
                  <tr>
                    <td class="text-center" style="width: 50px;"><?php //echo $i; ?></td>
                    <td style="width: 350px;"><img src="<?php echo base_url('themes/img/product/'); ?>" style="width: 100px; height: 100px; margin-right: 10px"> <?php //echo $row['nama_produk']; ?></td>
                    <td class="text-center" style="width: 50px;"><?php //echo $row['jumlah']; ?></td>
                    <td style="width: 100px">Rp <?php //echo number_format($row['harga']).'.00'; ?></td>
                    <?php //$sub_total=$row['harga']*$row['jumlah']; ?>
                    <td style="width: 100px">Rp <?php //echo number_format($sub_total).'.00'; ?></td>
                  </tr>
                  <?php //$i++; } ?>
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">Rincian Pengiriman:</p>
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:50%">Metode Pengiriman :</th>
                    <td><?php //echo strtoupper($invoice['type_pengiriman']); ?></td>
                  </tr>
                  <tr>
                    <th>Metode Pembayaran :</th>
                    <td><?php //echo ucfirst($invoice['type_pembayaran']); ?></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->

            <div class="col-xs-6">
              <p class="lead">Rincian Biaya :</p>
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:50%">Total Harga :</th>
                    <td>Rp <?php //echo number_format($invoice['total_harga']).'.00'; ?></td>
                  </tr>
                  <tr>
                    <th>Ongkos Kirim :</th>
                    <td>Rp <?php //echo number_format($invoice['ongkos_kirim']).'.00'; ?></td>
                  </tr>
                  <!-- <tr>
                    <th><p class="form-control-static">Shipping:</p></th>
                    <td><input type="text" value="$5.80" class="form-control"></td>
                  </tr> -->
                  <tr>
                    <th>Total Belanja :</th>
                    <td>Rp <?php //echo number_format($invoice['total_belanja']).'.00'; ?></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <a href="<?php echo site_url('admin/invoice_print/'); ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo site_url('admin/invoice_pdf/'); ?>" target="_blank" class="btn btn-default"><i class="fa fa-download"></i> PDF</a>
              <a href="<?php echo site_url('p_admin/ubah_status_order/'); ?>" class="btn btn-success pull-right"><i class="fa fa-edit"></i> Change Status to Lunas</a>
            </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>