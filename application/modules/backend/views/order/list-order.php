              <section class="col-lg-12">
                <!-- general form elements -->
                <div class="box box-none">
                  <div class="box-header with-border">
                    <h2 class="box-title"><i class="fa fa-cart-arrow-down"></i> Orders</h2>
                    <div class="pull-right">
                      <a class="btn-filter pull-right" role="button" data-toggle="collapse" href="#box-filter" aria-expanded="true"><i class="fa fa-filter"></i></a>
                    </div>
                  </div><!-- /.box-header -->
                  <div id="box-filter" class="collapse" role="tabpanel">
                    <div class="panel-body">
                      asdfdsf
                    
                      <div class="box-tools pull-right">
                        <div class="input-group" style="width: 150px;">
                          <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.box-filter -->
                  <div class="box-body">
                    <div class="row">
                      
                      <div class="col-md-12">
                        <table id="list-product" class="table table-striped">
                          <thead>
                            <tr>
                              <th width="30">#</th>
                              <th width="100">Order ID</th>
                              <th width="100">Email</th>
                              <th width="100">Date Order</th>
                              <th width="100">Total Belanja</th>
                              <th width="100" class="text-center">Status</th>
                              <th width="100" class="text-center">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td width="30" style="padding-left: 13px;"></td>
                              <td width="100">
                                <p class="name-list"># 24242</p>
                              </td>
                              <td width="100">
                                <p class="name-list">arifmatika@gmail.com</p>
                              </td>
                              <td width="100">07:00, 30-Dec-2016</td>
                              <td width="100">Rp 30.000.000,00</td>
                              <td width="100" class="text-center"><span class="label label-default">Pending</span></td>
                              <td width="100" class="text-center">
                                <ul class="list-inline icon-action">
                                  <li><a href="<?php echo site_url('admin/order/invoice'); ?>"><i class="fa fa-eye"></i></a></li>
                                </ul>
                              </td>
                            </tr>
                          </tfoot>
                        </table>                      
                      </div><!-- end-col -->

                    </div><!-- end-row -->
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <div class="col-md-12">
                      <?php //echo $pagination; ?>
                    </div>
                  </div><!-- /.box-footer -->
                </div><!-- /.box -->
              </section>