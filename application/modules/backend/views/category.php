        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- list-category -->
            <?php $this->load->view('category/list-category'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
