        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- detail-product -->
            <?php $this->load->view('product/detail-product'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
