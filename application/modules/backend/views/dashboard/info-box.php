            <section class="col-lg-12">
              <!-- Info boxes -->
              <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="fa fa-product-hunt"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Products</span>
                      <span class="info-box-number">90 <small>items</small></span>
                    </div><!-- /.info-box-content -->
                  </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="fa fa-cart-arrow-down"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Orders</span>
                      <span class="info-box-number">760</span>
                    </div><!-- /.info-box-content -->
                  </div><!-- /.info-box -->
                </div><!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="fa fa-edit"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Posts</span>
                      <span class="info-box-number">41,410</span>
                    </div><!-- /.info-box-content -->
                  </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="ion ion-ios-people-outline"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Customers</span>
                      <span class="info-box-number">2,000</span>
                    </div><!-- /.info-box-content -->
                  </div><!-- /.info-box -->
                </div><!-- /.col -->
              </div><!-- /.row-Info boxes -->
            </section><!-- full-col -->