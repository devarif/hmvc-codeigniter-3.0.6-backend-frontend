            <section class="col-lg-6">
              <!-- TABLE: LATEST MEMBERS -->
              <div class="box box-none">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest Members</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Email</th>
                          <th>Name</th>
                          <th>Jenis Kelamin</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>arifmatika@gmail.com</td>
                          <td>Arief Bruce</td>
                          <td>Laki-laki</td>
                          <td><span class="label label-success">Aktif</span></td>
                        </tr>
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="javascript::;" class="uppercase">View All Orders</a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </section>