        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">

            <!-- col-lg-6 --><!-- list-media -->
            <?php $this->load->view('media/list-media'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
