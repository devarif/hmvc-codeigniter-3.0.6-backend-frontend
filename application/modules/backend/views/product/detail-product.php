              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Description</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Spesification</a></li>
                    <li class="pull-left header"><i class="fa fa-product-hunt"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-product-id">Product ID</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="product-id" type="text" class="form-control" id="input-product-id" value="Enter product ID" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-title">Title</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="title" type="text" class="form-control" id="input-title" value="Enter title" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-price">Price</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="price" type="text" class="form-control" id="input-price" value="Enter price" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-stok">Stok</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="stok" type="text" class="form-control" id="input-stok" value="Enter stok" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-category">Category</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="category" type="text" class="form-control" id="input-category" value="Enter category" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="title" type="text" class="form-control" id="input-tag" value="Enter status" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-tag">Tag</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="tag" type="text" class="form-control" id="input-tag" value="Enter tag" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-meta-description">Meta Description</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <textarea name="meta-description" rows="3" class="form-control" id="input-meta-description" disabled="disabled">Meta Description</textarea>
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                          <div class="thumbnail">
                            <img src="<?php echo base_url('assets/img/post/no-photo.png') ?>">
                          </div>
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                      <div class="general row">
                        <div class="col-md-12">
                          <div id="description-product">Hello World</div>
                        </div>
                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                      <div class="general row">
                        <div class="col-md-12">
                          <div id="spesification-product">Hello World</div>
                        </div>
                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>