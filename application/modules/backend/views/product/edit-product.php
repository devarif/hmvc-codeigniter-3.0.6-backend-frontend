              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Description</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Spesification</a></li>
                    <li class="pull-left header"><i class="fa fa-product-hunt"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-product-id">Product ID</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="product-id" type="text" class="form-control" id="input-product-id" placeholder="Enter product ID" required="required">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-title">Title</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="title" type="text" class="form-control" id="input-title" placeholder="Enter title" required="required">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-price">Price</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input name="price" type="number" class="form-control" id="input-price" placeholder="Enter price" required="required">
                                <span class="input-group-addon">.00</span>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-stok">Stok</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <div class="input-group">
                                <input name="stok" type="number" class="form-control" id="input-stok" placeholder="Enter stok" required="required">
                                <span class="input-group-addon">items</span>
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-category">Category</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="category" class="form-control select2" id="input-category" style="width: 100%;" required="required">
                                <option value="Botol" selected="selected">News</option>
                                <option value="Inner">Article</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="status" class="form-control select2" id="input-status" style="width: 100%;" required="required">
                                <option value="Enabled" selected="selected">Enabled</option>
                                <option value="Disabled">Disabled</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-picture">Picture</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <div class="btn btn-default btn-file col-md-12" style="margin-bottom: 5px;">
                                <i class="fa fa-image" style="font-size: 20px;"></i>
                                <input name="picture" type="file" id="input-picture" required="required">
                              </div>
                              <p class="help-block"><small>Format Extension .JPG, .JPEG, .PNG, .GIF (Max: 10MB).</small></p>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="add-more-picture"></label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <button id="add-more-picture" class="btn btn-default" style="width: 100%;">Add More Picture</button>
                            </div>
                          </div>
                          <div id="wrapper-add-more-picture"></div><!-- more-picture -->
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-tag">Tag</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="tag" type="text" class="form-control" id="input-tag" placeholder="Enter tag">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-meta-description">Meta Description</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <textarea name="meta-description" rows="3" class="form-control" id="input-meta-description"></textarea>
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                          <div class="thumbnail">
                            <img src="<?php echo base_url('assets/img/post/no-photo.png') ?>">
                          </div>
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                      <div class="general row">
                        <div class="col-md-12">
                          <textarea name="description" id="description-product" rows="10" placeholder="Enter text"></textarea>
                        </div>
                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                      <div class="general row">
                        <div class="col-md-12">
                          <textarea name="spesification" id="spesification-product" rows="10" placeholder="Enter text"></textarea>
                        </div>
                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>