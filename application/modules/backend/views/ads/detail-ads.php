              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                    <li class="pull-left header"><i class="fa fa-industry"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-title">Name</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="title" type="text" class="form-control" id="input-title" placeholder="Enter title" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-date-begin">Date Begin</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="date-begin" type="text" class="form-control" id="input-date-begin" placeholder="Enter date begin" data-provide="datepicker" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-date-end">Date End</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="date-end" type="text" class="form-control" id="input-date-end" placeholder="Enter date end" data-provide="datepicker" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-link">Permalink</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <div class="input-group">
                                <span class="input-group-addon"><?php echo base_url(); ?></span>
                                <input name="permalink" type="text" class="form-control" placeholder="Enter permalink" disabled="disabled">
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-category">Category</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="category" type="text" class="form-control" id="input-category" placeholder="Enter category" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="status" type="text" class="form-control" id="input-status" placeholder="Enter status" disabled="disabled">
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                          <div class="thumbnail">
                            <img src="<?php echo base_url('assets/img/post/no-photo.png') ?>">
                          </div>
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>