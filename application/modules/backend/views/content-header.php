        <!-- Content Header (Page header) -->
        <section class="content-header">
          <?php 
          	$uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); 
          	if($uri_3==='add' OR $uri_3==='edit' OR $uri_3==='detail' OR $uri_3==='compose') {
          		//title none
          	}else{ ?>
          <h1>
            <?php echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?>
            <small>Page</small>
          </h1>
          <?php }//end-if-uri ?>

          <?php 
            $attr_1 = '<button class="btn btn-primary"><i class="fa fa-save"></i> Save</button><a onclick="history.go(-1);" class="btn btn-default" style="margin-left: 5px;"><i class="fa fa-arrow-left"></i> Back</a>';
            $attr_2 = '<button class="btn btn-primary"><i class="fa fa-save"></i> Save Change</button><a onclick="history.go(-1);" class="btn btn-default" style="margin-left: 5px;"><i class="fa fa-arrow-left"></i> Back</a>';
            $attr_3 = '<a href="'.site_url('admin/'.strtolower($uri_2).'/edit').'" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a><a onclick="history.go(-1);" class="btn btn-default" style="margin-left: 5px;"><i class="fa fa-arrow-left"></i> Back</a>';
            $attr_4 = '<button class="btn btn-primary"><i class="fa fa-send"></i> Send</button><a class="btn btn-default" style="margin-left: 5px;"><i class="fa fa-pencil"></i> Draft</a><a onclick="history.go(-1);" class="btn btn-default" style="margin-left: 5px;"><i class="fa fa-arrow-left"></i> Back</a>';
            if(!empty($uri_3)){
                switch ($uri_3) {
                    case 'add':
                        echo $attr_1;
                        break;
                    case 'edit':
                        echo $attr_2;
                        break;
                    case 'detail':
                        echo $attr_3;
                        break;
                    case 'compose':
                        echo $attr_4;
                        break;
                    default:
                        echo '';
                }
            }//end-if-!empty-uri_3 ?>

          <?php $this->load->view('breadcrumb.php'); ?>
        </section>