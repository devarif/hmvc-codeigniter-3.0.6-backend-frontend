        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- detail-page -->
            <?php $this->load->view('page/detail-page'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
