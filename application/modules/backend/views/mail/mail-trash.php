              <section class="col-lg-12">
                <!-- general form elements -->
                <div class="box box-none">
                  <div class="box-header with-border">
                    <a href="<?php echo site_url('admin/mail/compose'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Compose</a>
                    <a href="<?php echo site_url('admin/mail/delete_all'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
                    <a href="<?php echo site_url('admin/mail/inbox'); ?>" class="btn btn-default btn-sm"><i class="fa fa fa-refresh"></i> Refresh</a>
                    
                      <div class="pull-right">
                        <a class="btn-filter pull-right" role="button" data-toggle="collapse" href="#box-filter" aria-expanded="true"><i class="fa fa-filter"></i></a>
                      </div>
                  </div><!-- /.box-header -->
                  <div id="box-filter" class="collapse" role="tabpanel">
                    <div class="panel-body">
                      asdfdsf
                    
                      <div class="box-tools pull-right">
                        <div class="input-group" style="width: 150px;">
                          <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.box-filter -->
                  <div class="box-body">
                    <div class="row">
                      
                      <div class="col-md-12">
                        <table id="list-mail" class="table table-hover table-striped">
                          <thead>
                            <tr>
                              <th width="30"><a class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></a></th>
                              <th width="100">Email</th>
                              <th width="200">Subject</th>
                              <th width="300">Content</th>
                              <th width="100" class="text-center">Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td width="30" style="padding-left: 13px;"><input name="checkbox_<?php //echo $i; ?>" type="checkbox"><input name="id_mail_<?php //echo $i; ?>" value="<?php //echo $row['id_mail']; ?>" type="hidden"></td>
                              <td width="100">
                                <a href="<?php echo site_url('admin/mail/read'); ?>"><p class="name-list">arifmatika@gmail.com </p></a>
                              </td>
                              <td width="200"><a href="<?php echo site_url('admin/mail/read'); ?>"><p class="name-list">News</p></a></td>
                              <td width="300"><a href="<?php echo site_url('admin/mail/read'); ?>"><p class="name-list">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p></a></td>
                              <td width="100" class="text-center"><span>3 April 2016, 07:00</span></td>
                            </tr>
                            <tr>
                              <td width="30" style="padding-left: 13px;"><input name="checkbox_<?php //echo $i; ?>" type="checkbox"><input name="id_mail_<?php //echo $i; ?>" value="<?php //echo $row['id_mail']; ?>" type="hidden"></td>
                              <td width="100">
                                <a href="<?php echo site_url('admin/mail/read'); ?>"><p class="name-list">arifmatika@gmail.com </p></a>
                              </td>
                              <td width="200"><a href="<?php echo site_url('admin/mail/read'); ?>"><p class="name-list">News</p></a></td>
                              <td width="300"><a href="<?php echo site_url('admin/mail/read'); ?>"><p class="name-list">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p></a></td>
                              <td width="100" class="text-center"><span>3 April 2016, 07:00</span></td>
                            </tr>
                          </tfoot>
                        </table>                      
                      </div><!-- end-col -->

                    </div><!-- end-row -->
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <div class="col-md-12">
                      <?php //echo $pagination; ?>
                    </div>
                  </div><!-- /.box-footer -->
                </div><!-- /.box -->
              </section>