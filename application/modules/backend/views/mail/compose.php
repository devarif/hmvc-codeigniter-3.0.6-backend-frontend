              <section class="col-lg-12">
                <!-- general form elements -->
                <div class="box box-none">
                  <div class="box-header with-border">
                  	<h3 class="box-title"><i class="fa fa-envelope-o"></i> Compose</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      
                      <div class="col-md-12">
                      	<div class="form-group">
							<input name="email" type="email" class="form-control" id="input-email" placeholder="To :">
                      	</div>
                      	<div class="form-group">
							<input name="subject" type="text" class="form-control" id="input-subject" placeholder="Subject :">
                      	</div>
                      	<div class="form-group">
							<textarea name="content-post" id="content-post" rows="10" placeholder="Enter text"></textarea>
                      	</div>
                      </div><!-- end-col -->

                    </div><!-- end-row -->
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <div class="col-md-12">
                      <?php //echo $pagination; ?>
                    </div>
                  </div><!-- /.box-footer -->
                </div><!-- /.box -->
              </section>