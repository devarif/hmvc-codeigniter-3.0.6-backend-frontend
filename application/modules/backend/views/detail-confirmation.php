        <!-- Content Header (Page header) -->
        <section class="content-header">
        <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); ?>
          <h1>
            <?php echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?>
            <small>Page</small>
          </h1>

          <?php $this->load->view('breadcrumb.php'); ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- detail-confirmation -->
            <?php $this->load->view('confirmation/detail-confirmation'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
