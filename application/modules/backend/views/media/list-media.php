              <section class="col-lg-12">
                <!-- general form elements -->
                <div class="box box-none">
                  <div class="box-header with-border">
                    <a data-target="#modal-add-media" data-toggle="modal" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> New</a>
                    <a href="<?php echo site_url('admin/media/delete_all'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
                    
                      <div class="pull-right">
                        <a class="btn-filter pull-right" role="button" data-toggle="collapse" href="#box-filter" aria-expanded="true"><i class="fa fa-filter"></i></a>
                      </div>
                  </div><!-- /.box-header -->
                  <div id="box-filter" class="collapse" role="tabpanel">
                    <div class="panel-body">
                      asdfdsf
                    
                      <div class="box-tools pull-right">
                        <div class="input-group" style="width: 150px;">
                          <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.box-filter -->
                  <div class="box-body">
                    <div class="row">
                      
                      <div class="col-md-12">
                        <table id="list-product" class="table table-striped">
                          <thead>
                            <tr>
                              <th width="30"><a class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></a></th>
                              <th width="200">File</th>
                              <th width="200">Link</th>
                              <th width="100">Date Upload</th>
                              <th width="100" class="text-center">Status</th>
                              <th width="100" class="text-center">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td width="30" style="padding-left: 13px;"><input name="checkbox_<?php //echo $i; ?>" type="checkbox"><input name="id_mail_<?php //echo $i; ?>" value="<?php //echo $row['id_mail']; ?>" type="hidden"></td>
                              <td width="200">
                                <img src="<?php echo base_url('assets/img/post/no-photo.png') ?>" alt="<?php //echo $row['file']; ?>" width="60" height="60" style="float: left;margin-right: 10px;">
                                <p class="title-list">Lorem-Ipsum-Lorem.jpg</p>
                              </td>
                              <td width="200">News</td>
                              <td width="100">3 April 2016, 07:00</td>
                              <td width="100" class="text-center"><span class="label label-danger">Disable</span></td>
                              <td width="100" class="text-center">
                                <ul class="list-inline icon-action">
                                  <li><a href="<?php echo site_url('admin/media/detail'); ?>"><i class="fa fa-eye"></i></a></li>
                                  <li><a href="<?php echo site_url('admin/media/edit'); ?>"><i class="fa fa-edit"></i></a></li>
                                  <li><a href="<?php echo site_url('admin/media/delete'); ?>"><i class="fa fa-trash"></i></a></li>
                                </ul>
                              </td>
                            </tr>
                          </tfoot>
                        </table>                      
                      </div><!-- end-col -->

                    </div><!-- end-row -->
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <div class="col-md-12">
                      <?php //echo $pagination; ?>
                    </div>
                  </div><!-- /.box-footer -->
                </div><!-- /.box -->
              </section>

              <div class="modal fade" id="modal-add-media">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Add New Media</h4>
                    </div>
                    <?php echo form_open_multipart('p_admin/tambah_media/'); ?>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="btn btn-default btn-file col-md-12" style="margin-bottom: 15px;">
                              <i class="fa fa-upload" style="font-size: 75px;"></i>
                              <input name="file" type="file">
                            </div>
                            <p class="help-block">Format Extension .JPG, .JPEG, .PNG, .GIF (Max: 10MB).</p>
                          </div>
                          <div class="form-group">
                            <label>Status <i class="text-danger">*</i></label>
                            <select name="category" class="form-control select2" style="width: 100%;">
                              <option value="" selected="selected">Pilih Categories</option>
                              <?php foreach ($blog_categories as $row) { ?>
                              <option value="<?php echo $row['id_category'] ?>" disabled="disabled"><?php echo $row['category']; ?></option>
                              <!-- show-sub-categories -->
                              <?php $id=$row['id_category']; $sub_categories=$this->m_admin->tampil_sub_categories($id); ?>
                              <?php foreach ($sub_categories as $row) { ?>
                              <option value="<?php echo $row['id_category']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['category']; ?></option>
                              <?php }//end-sub-categories ?>
                              <?php }//end-blog_categories ?>

                              <!-- categories-catalog -->
                              <?php foreach ($product_categories as $row) { ?>
                              <option value="<?php echo $row['id_category']; ?>" disabled="disabled"><?php echo $row['category']; ?></option>
                              <!-- show-sub-categories -->
                              <?php $id=$row['id_category']; $sub_categories=$this->m_admin->tampil_sub_categories($id); ?>
                              <?php foreach ($sub_categories as $row) { ?>
                              <option value="<?php echo $row['id_category']; ?>" disabled="disabled">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['category']; ?></option>
                              <!-- show-sub-subcategories -->
                              <?php $id2=$row['id_category']; $sub_subcategories=$this->m_admin->tampil_sub_subcategories($id2); ?>
                              <?php foreach ($sub_subcategories as $row) { ?>
                              <option value="<?php echo $row['id_category']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <?php echo $row['category']; ?></option>
                              <?php }//end-foreach-sub-subcategories ?>
                              <?php }//end-foreach-sub-categories ?>
                              <?php }//end-foreach-product_categories ?>
                            </select>
                          </div><!-- /.form-group -->
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button name="upload" type="submit" class="btn btn-primary col-md-12">UPLOAD</button>
                    </div>
                    <?php echo form_close(); ?>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->