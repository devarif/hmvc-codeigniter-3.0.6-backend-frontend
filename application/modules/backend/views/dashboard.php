        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- Info boxes -->
            <?php $this->load->view('dashboard/info-box'); ?>

            <!-- col-lg-6 --><!-- recent-product -->
            <?php $this->load->view('dashboard/recent-product'); ?>

            <!-- col-lg-6 --><!-- latest-order -->
            <?php $this->load->view('dashboard/latest-order'); ?>

            <!-- col-lg-6 --><!-- recent-post -->
            <?php $this->load->view('dashboard/recent-post'); ?>

            <!-- col-lg-6 --><!-- latest-member -->
            <?php $this->load->view('dashboard/latest-member'); ?>

            <!-- col-lg-6 --><!-- sales-report -->
            <?php $this->load->view('dashboard/sales-report'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->