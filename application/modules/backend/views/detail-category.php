        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- detail-category -->
            <?php $this->load->view('category/detail-category'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
