              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Customer</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Address</a></li>
                    <li class="pull-left header"><i class="fa fa-user"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-name">Name</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="name" type="text" class="form-control" id="input-name" placeholder="Enter name" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-email">Email</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="email" type="text" class="form-control" id="input-email" placeholder="Enter email" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-telephone">Telephone</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="telephone" type="text" class="form-control" id="input-telephone" placeholder="Enter telephone" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="status" type="text" class="form-control" id="input-status" placeholder="Enter status" disabled="disabled">
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-title">Address</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <textarea name="description" class="form-control" rows="3" placeholder="Address" disabled="disabled"></textarea>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-district">District</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="district" type="text" class="form-control" id="input-district" value="Enter district" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-city">City</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="city" type="text" class="form-control" id="input-city" value="Enter city" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-state">State</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="state" type="text" class="form-control" id="input-state" value="Enter state" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-postcode">Postcode</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="postcode" type="text" class="form-control" id="input-postcode" value="Enter postcode" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-country">Country</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="country" type="text" class="form-control" id="input-country" value="Enter country" disabled="disabled">
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>