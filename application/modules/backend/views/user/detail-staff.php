              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Staff</a></li>
                    <li class="pull-left header"><i class="fa fa-user"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-name">Name</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="name" type="text" class="form-control" id="input-name" placeholder="Enter name" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-email">Email</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="email" type="email" class="form-control" id="input-email" placeholder="Enter email" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-password">Password</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="password" type="password" class="form-control" id="input-password" placeholder="Enter password" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-password">Password Confirm</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="password_confirmation" type="password" class="form-control" id="input-password" placeholder="Enter password confirmation" disabled="disabled">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="status" type="text" class="form-control" id="input-status" placeholder="Enter status" disabled="disabled">
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>