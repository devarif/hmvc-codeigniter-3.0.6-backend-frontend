              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Customer</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Address</a></li>
                    <li class="pull-left header"><i class="fa fa-user"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-name">Name</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="name" type="text" class="form-control" id="input-name" placeholder="Enter name">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-email">Email</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="email" type="text" class="form-control" id="input-email" placeholder="Enter email">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-telephone">Telephone</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="telephone" type="text" class="form-control" id="input-telephone" placeholder="Enter telephone">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="status" class="form-control select2" id="input-status" style="width: 100%;">
                                <option value="Enabled" selected="selected">Enabled</option>
                                <option value="Disabled">Disabled</option>
                              </select>
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-address">Address</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <textarea name="address" rows="3" class="form-control" id="input-address" placeholder="Enter address"></textarea>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-district">District</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="district" type="text" class="form-control" id="input-district" placeholder="Enter district">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-city">City</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="city" type="text" class="form-control" id="input-city" placeholder="Enter city">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-state">State</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="state" type="text" class="form-control" id="input-state" placeholder="Enter state">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-postcode">Postcode</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="postcode" type="text" class="form-control" id="input-postcode" placeholder="Enter postcode">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-country">Country</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="country" class="form-control select2" id="input-country" style="width: 100%;">
                                <option value="Enabled" selected="selected">Enabled</option>
                                <option value="Disabled">Disabled</option>
                              </select>
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>