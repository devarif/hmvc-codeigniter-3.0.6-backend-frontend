        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- mail-inbox -->
            <?php $this->load->view('mail/mail-inbox'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
