          <?php if($this->uri->segment(2) !== 'dashboard'){ ?>
          <?php
            $uri_2 = $this->uri->segment(2);
            $uri_3 = $this->uri->segment(3);
          ?>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/'.$uri_2); ?>"><i class="fa fa-dashboard"></i> <?php echo str_replace('-', ' ', ucfirst($uri_2)); ?></a></li>
            <?php 
            $attr_1 = '<li class="active">Add '.ucfirst($uri_2).'</li>';
            $attr_2 = '<li class="active">Edit '.ucfirst($uri_2).'</li>';
            $attr_3 = '<li class="active">Detail '.ucfirst($uri_2).'</li>';
            $attr_4 = '<li class="active">Invoice</li>';
            if(!empty($uri_3)){
                switch ($uri_3) {
                    case 'add':
                        echo $attr_1;
                        break;
                    case 'edit':
                        echo $attr_2;
                        break;
                    case 'detail':
                        echo $attr_3;
                        break;
                    case 'invoice':
                        echo $attr_4;
                        break;
                    default:
                        echo '';
                }
            }//end-if-!empty-uri_3 ?>
          </ol>
          <?php }//end-if-dashboard ?>