              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Category Detail</a></li>
                    <li class="pull-left header"><i class="fa fa-tag"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-title">Name</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="title" type="text" class="form-control" id="input-title" placeholder="Enter title">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-description">Description</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <textarea name="description" placeholder="Enter description..." class="form-control" rows="3"></textarea>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-parent">Parent</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="parent" class="form-control select2" id="input-parent" style="width: 100%;">
                                <option value="Botol" selected="selected">News</option>
                                <option value="Inner">Article</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="status" class="form-control select2" id="input-status" style="width: 100%;">
                                <option value="Enabled" selected="selected">Enabled</option>
                                <option value="Disabled">Disabled</option>
                              </select>
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>