      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php //echo base_url('themes/img/user/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div> -->
          <!-- search form -->
          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li class="treeview <?php if($this->uri->segment(2)==='dashboard'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/dashboard'); ?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='product'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/product'); ?>">
                <i class="fa fa-product-hunt"></i> <span>Products</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='order'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/order') ?>">
                <i class="fa fa-cart-arrow-down"></i> <span>Orders</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='confirmation'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/confirmation'); ?>">
                <i class="fa fa-sticky-note-o"></i> <span>Confirmations</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='media'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/media'); ?>">
                <i class="fa fa-upload"></i> <span>Media</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='post'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/post'); ?>">
                <i class="fa fa-pencil"></i> <span>Posts</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='category'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/category'); ?>">
                <i class="fa fa-tag"></i> <span>Categories</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='page'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/page'); ?>">
                <i class="fa fa-clone"></i> <span>Pages</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)==='customer' OR $this->uri->segment(2)==='staff'){echo 'active'; } ?>">
              <a href="#">
                <i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li <?php if($this->uri->segment(2)=='customer'){ echo 'class="active"'; } ?> ><a href="<?php echo site_url('admin/customer'); ?>"><i class="fa fa-circle-o"></i> Customers</a></li>
                <li <?php if($this->uri->segment(2)=='staff'){ echo 'class="active"'; } ?> ><a href="<?php echo site_url('admin/staff'); ?>"><i class="fa fa-circle-o"></i> Staff</a></li>
              </ul>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)=='mail'){echo 'active'; } ?>">
              <a href="#">
                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li <?php if($this->uri->segment(3)=='compose'){ echo 'class="active"'; } ?> ><a href="<?php echo site_url('admin/mail/compose'); ?>"><i class="fa fa-circle-o"></i>Compose</a></li>
                <li <?php if($this->uri->segment(3)=='inbox'){ echo 'class="active"'; } ?> ><a href="<?php echo site_url('admin/mail/inbox'); ?>"><i class="fa fa-circle-o"></i>Inbox <span class="label label-primary pull-right"></span></a></li>
                <li <?php if($this->uri->segment(3)=='sent'){ echo 'class="active"'; } ?> ><a href="<?php echo site_url('admin/mail/sent'); ?>"><i class="fa fa-circle-o"></i>Sent</a></li>
                <li <?php if($this->uri->segment(3)=='drafts'){ echo 'class="active"'; } ?> ><a href="<?php echo site_url('admin/mail/drafts'); ?>"><i class="fa fa-circle-o"></i>Drafts</a></li>
                <li <?php if($this->uri->segment(3)=='trash'){ echo 'class="active"'; } ?> ><a href="<?php echo site_url('admin/mail/trash'); ?>"><i class="fa fa-circle-o"></i>Trash</a></li>
              </ul>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)=='ads'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/ads'); ?>">
                <i class="fa fa-industry"></i> <span>Ads</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)=='slide'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/slide'); ?>">
                <i class="fa fa-image"></i> <span>Slides</span>
              </a>
            </li>
            <li class="treeview <?php if($this->uri->segment(2)=='setting'){echo 'active'; } ?>">
              <a href="<?php echo site_url('admin/setting'); ?>">
                <i class="fa fa-wrench"></i> <span>Settings</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">