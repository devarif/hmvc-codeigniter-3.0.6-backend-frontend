        <!-- Content Header (Page header) -->
        <?php $this->load->view('content-header'); ?>

        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- col-lg-12 --><!-- edit-page -->
            <?php $this->load->view('page/edit-page'); ?>

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
