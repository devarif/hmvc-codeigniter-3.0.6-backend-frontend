              <section class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Content</a></li>
                    <li class="pull-left header"><i class="fa fa-product-hunt"></i> <?php $uri_2 = $this->uri->segment(2); $uri_3 = $this->uri->segment(3); echo str_replace('-', ' ', ucfirst($uri_3) ).' '.str_replace('-', ' ', ucfirst($uri_2) ); ?></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      <div class="general row">
                        
                        <div class="col-md-8">
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-title">Title</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="title" type="text" class="form-control" id="input-title" placeholder="Enter title">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-layout">Layout</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="layout" class="form-control select2" id="input-layout" style="width: 100%;">
                                <option value="Botol" selected="selected">News</option>
                                <option value="Inner">Article</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-language">Language</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="language" class="form-control select2" id="input-language" style="width: 100%;">
                                <option value="Botol" selected="selected">News</option>
                                <option value="Inner">Article</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-link">Permalink</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <div class="input-group">
                                <span class="input-group-addon"><?php echo base_url(); ?></span>
                                <input name="permalink" type="text" class="form-control" placeholder="Enter permalink">
                              </div>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-status">Status</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <select name="status" class="form-control select2" id="input-status" style="width: 100%;">
                                <option value="Enabled" selected="selected">Enabled</option>
                                <option value="Disabled">Disabled</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-tag">Tag</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <input name="tag" type="text" class="form-control" id="input-tag" placeholder="Enter tag">
                            </div>
                          </div>
                          <div class="form-group col-md-12">
                            <div class="col-md-4">
                              <label for="input-meta-description">Meta Description</label><span class="pull-right"></span>
                            </div>
                            <div class="col-md-8">
                              <textarea name="meta-description" rows="3" class="form-control" id="input-meta-description"></textarea>
                            </div>
                          </div>
                        </div><!-- /.col-8 -->
                        <div class="col-md-4">
                        </div><!-- /.col-4 -->

                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                      <div class="general row">
                        <div class="col-md-12">
                          <textarea name="content-page" id="content-page" rows="10" placeholder="Enter text">Content page</textarea>
                        </div>
                      </div><!-- /.general -->
                    </div><!-- /.tab-pane -->
                  </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
              </section>