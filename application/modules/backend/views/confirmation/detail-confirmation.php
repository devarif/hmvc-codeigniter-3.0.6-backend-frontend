            <!-- full-col -->
            <section class="col-md-12">
              <!-- Horizontal Form -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-sticky-note-o"></i> Detail Confirmation</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">Order ID <span class="pull-right"><span class="pull-right">:</span></span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static"># <?php echo ''; ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Name <span class="pull-right">:</span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo ''; ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">No Rekening <span class="pull-right">:</span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo ''; ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Bank Asal <span class="pull-right">:</span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo ''; ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Total Transfer <span class="pull-right">:</span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static">Rp <?php //echo number_format($detail_confirmation['total_transfer']).',00'; ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Date Transfer <span class="pull-right">:</span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo ''; ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Bank Tujuan <span class="pull-right">:</span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo ''; ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Notes <span class="pull-right">:</span></label>
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo ''; ?></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <a href=""><img src="<?php echo base_url('themes/img/invoice/'); ?>" style="width:100%;"></a>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </section><!-- full-col -->