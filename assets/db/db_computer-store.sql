-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2016 at 08:36 
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_computer-store`
--

-- --------------------------------------------------------

--
-- Table structure for table `db_categories`
--

CREATE TABLE IF NOT EXISTS `db_categories` (
  `id_category` bigint(11) NOT NULL,
  `tgl_upload` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_category_translation`
--

CREATE TABLE IF NOT EXISTS `db_category_translation` (
  `id_category_translation` bigint(11) NOT NULL,
  `id_category` bigint(11) NOT NULL,
  `id_language` bigint(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `description_category_translation` longtext NOT NULL,
  `type_category_translation` varchar(20) NOT NULL,
  `parent` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_confirmations`
--

CREATE TABLE IF NOT EXISTS `db_confirmations` (
  `id_confirmations` bigint(11) NOT NULL,
  `kode_order` varchar(20) NOT NULL,
  `nama_rekening` varchar(200) NOT NULL,
  `no_rekening` varchar(30) NOT NULL,
  `bank_rekening` varchar(20) NOT NULL,
  `total_transfer` varchar(20) NOT NULL,
  `tgl_transfer` datetime NOT NULL,
  `bank_tujuan` varchar(20) NOT NULL,
  `catatan` text NOT NULL,
  `image_konfirmasi` text NOT NULL,
  `status_konfirmasi` varchar(20) NOT NULL,
  `tgl_konfirmasi` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_images`
--

CREATE TABLE IF NOT EXISTS `db_images` (
  `id_image` bigint(11) NOT NULL,
  `tgl_upload` datetime NOT NULL,
  `title_image` text NOT NULL,
  `content_image` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_mail`
--

CREATE TABLE IF NOT EXISTS `db_mail` (
  `id_mail` bigint(11) NOT NULL,
  `id_user` bigint(11) NOT NULL,
  `mail_to` varchar(100) NOT NULL,
  `mail_from` varchar(100) NOT NULL,
  `subject_mail` text NOT NULL,
  `content_mail` longtext NOT NULL,
  `type_mail` varchar(20) NOT NULL,
  `tgl_kirim` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_orders`
--

CREATE TABLE IF NOT EXISTS `db_orders` (
  `id_order` bigint(11) NOT NULL,
  `kode_order` varchar(20) NOT NULL,
  `status_order` varchar(20) NOT NULL,
  `type_pembayaran` varchar(20) NOT NULL,
  `type_pengiriman` varchar(20) NOT NULL,
  `total_harga` varchar(20) NOT NULL,
  `ongkos_kirim` varchar(20) NOT NULL,
  `total_belanja` varchar(20) NOT NULL,
  `tgl_transaksi` datetime NOT NULL,
  `id_user` bigint(11) NOT NULL,
  `id_shipping_address` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_order_detail`
--

CREATE TABLE IF NOT EXISTS `db_order_detail` (
  `id_order_detail` bigint(11) NOT NULL,
  `id_order` bigint(11) NOT NULL,
  `id_product` bigint(11) NOT NULL,
  `jumlah` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_posts`
--

CREATE TABLE IF NOT EXISTS `db_posts` (
  `id_post` bigint(11) NOT NULL,
  `tgl_post` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_post_translation`
--

CREATE TABLE IF NOT EXISTS `db_post_translation` (
  `id_post_translation` bigint(11) NOT NULL,
  `id_category` bigint(11) NOT NULL,
  `id_language` bigint(11) NOT NULL,
  `title_post` text NOT NULL,
  `content_post` longtext NOT NULL,
  `image_post` text NOT NULL,
  `status_post` varchar(20) NOT NULL,
  `type_post` varchar(20) NOT NULL,
  `tag_post` text NOT NULL,
  `meta_description_post` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_products`
--

CREATE TABLE IF NOT EXISTS `db_products` (
  `id_product` bigint(11) NOT NULL,
  `kode_product` varchar(20) NOT NULL,
  `tgl_upload` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_product_translation`
--

CREATE TABLE IF NOT EXISTS `db_product_translation` (
  `id_product_translation` bigint(11) NOT NULL,
  `id_product` bigint(11) NOT NULL,
  `id_image` bigint(11) NOT NULL,
  `id_category` bigint(11) NOT NULL,
  `id_language` bigint(11) NOT NULL,
  `nama_product` text NOT NULL,
  `harga` varchar(20) NOT NULL,
  `stok` varchar(20) NOT NULL,
  `berat` varchar(20) NOT NULL,
  `volume` varchar(20) NOT NULL,
  `panjang` varchar(20) NOT NULL,
  `lebar` varchar(20) NOT NULL,
  `tinggi` varchar(20) NOT NULL,
  `pembeli` int(20) NOT NULL,
  `pengunjung` int(20) NOT NULL,
  `deskripsi_product` longtext NOT NULL,
  `spesifikasi_product` longtext NOT NULL,
  `tag_product` text NOT NULL,
  `meta_description_product` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_shipping_address`
--

CREATE TABLE IF NOT EXISTS `db_shipping_address` (
  `id_shipping_address` bigint(11) NOT NULL,
  `nama_customer` varchar(200) NOT NULL,
  `telephone_customer` varchar(20) NOT NULL,
  `alamat_customer` text NOT NULL,
  `kode_pos` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_users`
--

CREATE TABLE IF NOT EXISTS `db_users` (
  `id_user` bigint(11) NOT NULL,
  `id_fb` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(300) NOT NULL,
  `nama_depan` varchar(100) NOT NULL,
  `nama_belakang` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `subscribe` varchar(20) NOT NULL,
  `status_user` varchar(20) NOT NULL,
  `type_user` varchar(20) NOT NULL,
  `tgl_daftar` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `db_categories`
--
ALTER TABLE `db_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `db_category_translation`
--
ALTER TABLE `db_category_translation`
  ADD PRIMARY KEY (`id_category_translation`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_language` (`id_language`);

--
-- Indexes for table `db_confirmations`
--
ALTER TABLE `db_confirmations`
  ADD PRIMARY KEY (`id_confirmations`);

--
-- Indexes for table `db_images`
--
ALTER TABLE `db_images`
  ADD PRIMARY KEY (`id_image`);

--
-- Indexes for table `db_mail`
--
ALTER TABLE `db_mail`
  ADD PRIMARY KEY (`id_mail`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `db_orders`
--
ALTER TABLE `db_orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_shipping_address` (`id_shipping_address`);

--
-- Indexes for table `db_order_detail`
--
ALTER TABLE `db_order_detail`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `db_posts`
--
ALTER TABLE `db_posts`
  ADD PRIMARY KEY (`id_post`);

--
-- Indexes for table `db_post_translation`
--
ALTER TABLE `db_post_translation`
  ADD PRIMARY KEY (`id_post_translation`);

--
-- Indexes for table `db_products`
--
ALTER TABLE `db_products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `db_product_translation`
--
ALTER TABLE `db_product_translation`
  ADD PRIMARY KEY (`id_product_translation`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_image` (`id_image`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_language` (`id_language`);

--
-- Indexes for table `db_shipping_address`
--
ALTER TABLE `db_shipping_address`
  ADD PRIMARY KEY (`id_shipping_address`);

--
-- Indexes for table `db_users`
--
ALTER TABLE `db_users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `db_categories`
--
ALTER TABLE `db_categories`
  MODIFY `id_category` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_category_translation`
--
ALTER TABLE `db_category_translation`
  MODIFY `id_category_translation` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_confirmations`
--
ALTER TABLE `db_confirmations`
  MODIFY `id_confirmations` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_images`
--
ALTER TABLE `db_images`
  MODIFY `id_image` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_mail`
--
ALTER TABLE `db_mail`
  MODIFY `id_mail` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_orders`
--
ALTER TABLE `db_orders`
  MODIFY `id_order` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_order_detail`
--
ALTER TABLE `db_order_detail`
  MODIFY `id_order_detail` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_posts`
--
ALTER TABLE `db_posts`
  MODIFY `id_post` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_post_translation`
--
ALTER TABLE `db_post_translation`
  MODIFY `id_post_translation` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_products`
--
ALTER TABLE `db_products`
  MODIFY `id_product` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_product_translation`
--
ALTER TABLE `db_product_translation`
  MODIFY `id_product_translation` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_shipping_address`
--
ALTER TABLE `db_shipping_address`
  MODIFY `id_shipping_address` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `db_users`
--
ALTER TABLE `db_users`
  MODIFY `id_user` bigint(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
