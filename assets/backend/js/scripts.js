$(document).ready(function(){
  // add-more-picture
  var total = 1;
  $("#add-more-picture").click(function(){
    if (total<=9) {
      total = total + 1;
      $("#wrapper-add-more-picture").append('<div class="form-group col-md-12">'+
                                              '<div class="col-md-4">'+
                                                '<label for="input-picture-'+total+'">Picture '+total+'</label><span class="pull-right"></span>'+
                                              '</div>'+
                                              '<div class="col-md-8">'+
                                                '<div class="btn btn-default btn-file col-md-12" style="margin-bottom: 5px;">'+
                                                  '<i class="fa fa-image" style="font-size: 20px;"></i>'+
                                                  '<input name="picture-'+total+'" type="file" id="input-picture-'+total+'">'+
                                                '</div>'+
                                              '</div>'+
                                            '</div>');
    }
    return false;
  });
  // end-add-more-picture
});